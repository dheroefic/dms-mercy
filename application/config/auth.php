<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['auth_login_route']  = 'login';

$config['auth_logout_route'] = 'logout';

$config['auth_login_route_redirect'] = 'dashboard';

$config['auth_logout_route_redirect'] = 'login';

$config['auth_route_auto_redirect'] = [];

$config['auth_form_username_field'] = 'username';

$config['auth_form_password_field'] = 'password';

$config['auth_session_var'] = 'auth';