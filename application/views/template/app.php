<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<html lang="id">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="id">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><?php echo isset($title) ? APP_NAME . ' - ' . $title : 'Dashboard Management System' ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
    <meta name="description" content="This is an example dashboard created using build-in elements and components.">
    <meta name="msapplication-tap-highlight" content="no">
    <?php include 'components/css.php' ?>
</head>

<body>
    <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
        <?php include 'components/header.php' ?>
        <div class="app-main">
            <?php include 'components/sidebar.php' ?>
            <div class="app-main__outer">
                <div class="app-main__inner">
                    <?php if(isset($content)) echo $content ?>
                    <div class="app-wrapper-footer">
                        <?php include 'components/footer.php' ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include 'components/script.php' ?>
</body>

</html>