<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="app-sidebar sidebar-shadow">
    <div class="app-header__logo">
        <div class="logo-src"></div>
        <div class="header__pane ml-auto">
            <div>
                <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
        </div>
    </div>
    <div class="app-header__mobile-menu">
        <div>
            <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                </span>
            </button>
        </div>
    </div>
    <div class="app-header__menu">
        <span>
            <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                <span class="btn-icon-wrapper">
                    <i class="fa fa-ellipsis-v fa-w-6"></i>
                </span>
            </button>
        </span>
    </div>
    <div class="scrollbar-sidebar">
        <div class="app-sidebar__inner">
            <ul class="vertical-nav-menu">
                <li class="app-sidebar__heading">Dashboards</li>
                <li>
                    <a href="<?php echo base_url('dashboard') ?>" class="mm-active">
                        <i class="metismenu-icon pe-7s-rocket"></i> Dashboard
                    </a>
                </li>
                <!-- Master Data -->
                <li class="app-sidebar__heading">Master Data</li>
                <li>
                    <a href="<?php echo base_url('/') ?>">
                        <i class="metismenu-icon pe-7s-rocket"></i> Users
                    </a>
                </li>
                <!-- End of Master Data -->
                <!-- Transaction Menu -->
                <li class="app-sidebar__heading">Transaction</li>
                <li>
                    <a href="<?php echo base_url('/') ?>">
                        <i class="metismenu-icon pe-7s-rocket"></i> Data
                    </a>
                </li>
                <!-- End of Transaction Menu -->
                <!-- Report Menu -->
                <li class="app-sidebar__heading">Report</li>
                <li>
                    <a href="<?php echo base_url('/') ?>">
                        <i class="metismenu-icon pe-7s-rocket"></i> Data
                    </a>
                </li>
                <!-- End of Report Menu -->
                <li class="app-sidebar__heading">Logout</li>
                <li>
                    <a href="<?php echo route('logout') ?>">
                        <i class="metismenu-icon pe-7s-graph2"></i> Logout
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>