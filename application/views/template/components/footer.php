<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="app-footer">
    <div class="app-footer__inner">
        <div class="app-footer-left">
            <ul class="nav">
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <?php echo date('Y') ?> - Dashboard Management System
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>