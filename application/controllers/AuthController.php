<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Luthier\Auth\ControllerInterface;
use Luthier\Auth\Exception\UserNotFoundException;
use Luthier\Auth\Exception\InactiveUserException;
use Luthier\Auth\Exception\UnverifiedUserException;

class AuthController extends CI_Controller implements ControllerInterface
{
    private $userProvider;

    public function __construct() {
        parent::__construct();
        $this->userProvider = Auth::loadUserProvider($this->getUserProvider());
    }

    public function getUserProvider()
    {
        return 'UserProvider';
    }

    public function getMiddleware()
    {
        return 'AuthMiddleware';
    }

    public function login()
    {
        $this->load->view('auth/login');
    }

    public function logout()
    {
        Auth::destroy();
        return redirect(route('login'));
    }

    public function signup()
    {
        return;
    }

    public function emailVerification($token)
    {
        return;
    }

    public function passwordReset()
    {
        return;
    }

    public function passwordResetForm($token)
    {
        return;
    }

    public function authentication() {
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        try {
            $user = Auth::attempt($username, $password, $this->getUserProvider());
            Auth::store($user);
        }
        catch(UserNotFoundException $e)
        {
            $this->session->set_flashdata('error_auth_message', 'Either username or password is not valid.');
            return redirect(route('login'));
        }
        catch(InactiveUserException $e)
        {
            $this->session->set_flashdata('error_auth_message', 'This user is inactive.');
            return redirect(route('login'));
        }

        return redirect(route('dashboard'));
    }
}