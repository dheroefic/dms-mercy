<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DashboardController extends CI_Controller {
	
	public function index() {
		$content = $this->load->view('dashboard', [], true);
		return $this->load->view('template/app', [
			'title' => 'Dashboard',
			'content' => $content
		]);
	}

}