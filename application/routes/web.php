<?php

/**
 * Welcome to Luthier-CI!
 *
 * This is your main route file. Put all your HTTP-Based routes here using the static
 * Route class methods
 *
 * Examples:
 *
 *    Route::get('foo', 'bar@baz');
 *      -> $route['foo']['GET'] = 'bar/baz';
 *
 *    Route::post('bar', 'baz@fobie', [ 'namespace' => 'cats' ]);
 *      -> $route['bar']['POST'] = 'cats/baz/foobie';
 *
 *    Route::get('blog/{slug}', 'blog@post');
 *      -> $route['blog/(:any)'] = 'blog/post/$1'
 */

Route::get('login', 'AuthController@login')->name('login');
Route::post('auth', 'AuthController@authentication')->name('auth');
Route::get('logout', 'AuthController@logout')->name('logout');

Route::get('/', 'DashboardController@index', ['middleware' => 'Authenticated'])->name('dashboard');

Route::group('masterdata', ['middleware' => 'Authenticated'], function(){
	
});

Route::group('transaction', ['middleware' => 'Authenticated'], function(){
	
});

Route::group('report', ['middleware' => 'Authenticated'], function(){
	
});

Route::set('404_override', function(){
    return ci()->load->view('template/errors/404');
});

Route::set('translate_uri_dashes', FALSE);