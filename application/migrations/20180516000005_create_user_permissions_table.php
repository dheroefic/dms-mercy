<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_create_user_permissions_table extends CI_Migration
{
    public function up()
    {
        $this->dbforge->add_field([
            'id' =>
                [
                    'type'           => 'INT',
                    'constraint'     => 8,
                    'unsigned'       => TRUE,
                    'auto_increment' => TRUE
                ],
            'user_id' =>
                [
                    'type'       => 'INT',
                    'constraint' => 8,
                    'unsigned'   => TRUE,
                ],
            'category_id' =>
                [
                    'type'       => 'INT',
                    'constraint' => 8,
                    'unsigned'   => TRUE,
                ],

            'created_at DATETIME default CURRENT_TIMESTAMP',
            
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_field('CONSTRAINT FOREIGN KEY (user_id) REFERENCES tbl_users(id)');
        $this->dbforge->add_field('CONSTRAINT FOREIGN KEY (category_id) REFERENCES tbl_user_permissions_categories(id)');
        $this->dbforge->create_table('tbl_user_permissions');
    }

    public function down()
    {
        $this->dbforge->drop_table('tbl_user_permissions', TRUE);
    }
}