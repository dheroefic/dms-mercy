<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_create_users_table extends CI_Migration
{
    public function up()
    {
        $this->dbforge->add_field([
            'id' =>
                [
                    'type'           => 'INT',
                    'constraint'     => 8,
                    'unsigned'       => TRUE,
                    'auto_increment' => TRUE
                ],
            'full_name' =>
                [
                    'type'       => 'VARCHAR',
                    'constraint' => 100,
                ],
            'username' =>
                [
                    'type'       => 'VARCHAR',
                    'constraint' => 30,
                    'unique'     => TRUE,
                ],
            'email' =>
                [
                    'type'       => 'VARCHAR',
                    'constraint' => 255,
                    'unique'     => TRUE,
                ],
            'password' =>
                [
                    'type'       => 'VARCHAR',
                    'constraint' => 255,
                ],
            'role' =>
                [
                    'type'       => 'VARCHAR',
                    'constraint' => 45,
                    'default'    => 'user'
                ],
            'remember_token' =>
                [
                    'type'       => 'VARCHAR',
                    'constraint' => 128,
                ],
            'active' =>
                [
                    'type'       => 'INT',
                    'constraint' => 1,
                    'unsigned'   => TRUE,
                    'default'    => 1,
                ],
            'verified' =>
                [
                    'type'       => 'INT',
                    'constraint' => 1,
                    'unsigned'   => TRUE,
                    'default'    => 1,
                ],

            'created_at DATETIME default CURRENT_TIMESTAMP',
            'updated_at DATETIME default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('tbl_users');
        $this->db->insert('tbl_users', [
            'full_name' => 'Administrator',
            'username' => 'admin',
            'email' => 'admin@xyz.co',
            'password' => password_hash('demo', PASSWORD_DEFAULT),
            'role' => 'user'
        ]);
    }

    public function down()
    {
        $this->dbforge->drop_table('tbl_users', TRUE);
    }
}