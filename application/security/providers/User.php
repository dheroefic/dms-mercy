<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

use Luthier\Auth\UserInterface;

class User implements UserInterface {

	private $user;

	private $roles;

	private $permissions;

    public function __construct($entity, $roles, $permissions) {
        $this->user        = $entity;
        $this->roles       = $roles;
        $this->permissions = $permissions;
    }

    public function getEntity() {
        return $this->user;
    }

    public function getUsername() {
        return $this->user->email;
    }

    public function getRoles() {
        return $this->roles;
    }

    public function getPermissions() {
        return $this->permissions;
    }

}