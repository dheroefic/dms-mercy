<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

use Luthier\Auth\UserInterface;
use Luthier\Auth\UserProviderInterface;
use Luthier\Auth\Exception\UserNotFoundException;
use Luthier\Auth\Exception\InactiveUserException;
use Luthier\Auth\Exception\UnverifiedUserException;

class UserProvider implements UserProviderInterface
{
	public function getUserClass()
	{
		return 'User';
	}
	
	public function loadUserByUsername($username, $password = null)
	{
		$user = ci()->db->select('full_name, username, email, password, role, active, verified')
						->where('username', $username)
						->limit(1)
						->get('tbl_users')
						->row();

		if(empty($user))
		{
			throw new UserNotFoundException('Invalid user credentials!');
		}

		if($password !== NULL)
		{
			if(!$this->verifyPassword($password, $user->password))
			{
				throw new UserNotFoundException('Invalid user credentials!');
			}
		}

		unset($user->password);
		
		$userClass = $this->getUserClass();

		return new $userClass(
			/*  User data   */ $user,
			/*     Roles    */ $user->role,
			/*  Permissions */ []
		);
	}

	public function hashPassword($password)
	{
		return password_hash($password, PASSWORD_DEFAULT);
	}

	public function verifyPassword($password, $hash)
	{
		return password_verify($password, $hash);
	}

	final public function checkUserIsActive(UserInterface $user)
	{
        /*
         * The getEntity() method is used to return an array / object / entity with the
         * user data. In our case, it is an object, so we can use
         * the following chained syntax:
         */
        if($user->getEntity()->active == 0)
        {
        	throw new InactiveUserException();
        }
    }


    final public function checkUserIsVerified(UserInterface $user)
    {
        /*
         * The same here:
         */
        if($user->getEntity()->verified == 0)
        {
        	throw new UnverifiedUserException();
        }
    }
}
