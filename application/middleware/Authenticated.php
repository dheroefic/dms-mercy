<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Luthier\MiddlewareInterface;

class Authenticated implements MiddlewareInterface {

	public function run($args) {
		if( Auth::isGuest() ) {
			ci()->session->set_flashdata('error_auth_message', 'Your credential is already expired or You\'re not logged in.');
			return redirect(route('login'));		
		}
	}
	
}