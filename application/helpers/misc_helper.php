<?php

/**
 * Add csrf field to the form
 * @return void
 */
function csrf_field() {
	$name = ci()->security->get_csrf_token_name();
	$hash = ci()->security->get_csrf_hash();
	echo "<input type='hidden' name='$name' value='$hash' />";
}