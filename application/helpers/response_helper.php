<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Response builder for Rest API
 * @param string $status 
 * @param string $message 
 * @param array $data 
 * @return json response
 */
function responseBuilder($status, $message, $data=[]) {
	header('Content-Type: application/json');
	header('Access-Control-Allow-Origin: *');
	$response = [
		'status' => $status,
		'message' => $message
	];
	if (count($data) > 0) {
		$response['data'] = $data;
	}
	return print_r(json_encode($response));
}