<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Flatten multidimensional array to single array
 * @param array $items multidimensional array 
 * @return array
 */
function array_flatten($items)
{
    if (! is_array($items)) {
        return [$items];
    }
    return array_reduce($items, function ($carry, $item) {
        return array_merge($carry, array_flatten($item));
    }, []);
}
